import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";

export default function RenderOrders({ books }) {
  console.log("books ++++", books);
  return books.map(book => {
    return (
      <Card
        title={book.title}
        extra={<Link to={`/${book.id}`}> View</Link>}
        style={{ margin: 20 }}
      >
        <p>Card content</p>
      </Card>
    );
  });
}
