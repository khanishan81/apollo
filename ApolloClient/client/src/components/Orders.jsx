import React, { Component } from "react";
import PropTypes from "prop-types";
import { version, Button } from "antd";
import { connect } from "react-redux";
import { initState } from "../store/action";
import { Query, graphql } from "react-apollo";
import gql from "graphql-tag";
import RenderOrders from "./RenderOrders";

const ordersQuery = gql`
  {
    books {
      id
      title
      author {
        id
        name
      }
    }
  }
`;
export class Orders extends Component {
  state = {};

  componentDidMount() {}

  render() {
    console.log("Props ++++++++", this.props);
    if (this.props.data.loading) {
      return <h1> Loading Data.......</h1>;
    } else {
      this.props.onInitState(this.props.data.books);
      return (
        <div>
          <h1>Order Component </h1>
          {console.log(this.props.data.books)}
          <RenderOrders {...this.props.data} />
        </div>
      );
    }
  }
}

const mapStatetoProps = state => {
  if (!state) {
    console.log("Loading in map state component");
  } else {
    console.log("State got in component +++++++", state);
    return {
      orders: state.orders
    };
  }
};

const mapDispatchtoProps = dispatch => {
  return {
    onInitState: orders => dispatch(initState(orders))
  };
};

const cont = connect(
  mapStatetoProps,
  mapDispatchtoProps
)(Orders);

export default graphql(ordersQuery)(cont);
