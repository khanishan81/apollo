const initstate = {
  hello: "Hello World"
};

const appReducer = (state = initstate, action) => {
  switch (action.type) {
    case "save":
      console.log("Save Action called");
      const { data } = action;
      const newstate = {
        orders: data
      };
      return newstate;
      break;

    default:
      break;
  }
};

export default appReducer;
