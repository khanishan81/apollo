import express from "express";
import { ApolloServer, gql } from "apollo-server-express";
import { express as voyagerMiddleware } from "graphql-voyager/middleware";
import { SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG } from "constants";

const app = express();

const books = [
  {
    id: 1,
    title: "Harry Potter and the Chamber of Secrets",
    authorId: 1
  },
  {
    id: 2,
    title: "Jurassic Park",
    authorId: 2
  },
  {
    id: 3,
    title: "The Wolf of wall street",
    authorId: 2
  },
  {
    id: 4,
    title: "The yoga Book",
    authorId: 1
  }
];

const authors = [
  {
    id: 1,
    name: "Author One"
  },
  {
    id: 2,
    name: "Author two"
  }
];

const typeDefs = gql`
  # This "Book" type can be used in other type declarations.
  type Book {
    id: ID
    title: String
    author: Author
  }

  type Author {
    id: Int
    name: String
  }
  union Result = Book | Author
  type Query {
    books: [Book]
    serach(key: String): result
  }

  type Mutation {
    addbook(name: String): String
  }
`;

const resolvers = {
  Query: {
    books: () => books
  },
  Mutation: {
    addbook: (parent, args) => {
      return `Added book with name ${args.name}`;
    }
  },
  Book: {
    author: (book, args) => {
      let a = authors.filter(author => {
        return author.id == book.authorId;
      });
      console.log("Author =++++", a);
      return a[0];
    }
  },
  Result: {
    __resolveType(obj, context, info) {
      if (obj.name) {
        return "Author";
      }

      if (obj.title) {
        return "Book";
      }

      return null;
    }
  }
};
const server = new ApolloServer({
  typeDefs,
  resolvers
});

app.use("/voyager", voyagerMiddleware({ endpointUrl: "/graphql" }));

server.applyMiddleware({ app, path: "/graphql" });

app.listen({ port: 8000 }, () => {
  console.log("Apollo Server on http://localhost:8000/graphql");
});
